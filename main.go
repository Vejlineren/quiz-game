package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

func main()  {

	// FLAGS //
	// Use flag package to setup the CLI to accept -csv as a flag, followed by a CSV file string in correct format
	csvFilename := flag.String("csv", "problems.csv", "a csv file in the format of 'question,answer'")
	// Use -time flag to adjust timer
	timeLimit := flag.Int("time", 30, "Adjust time in seconds (Default is 30)")
	// Use -shuffle flag to shuffle the question order before start
	shuffleQs := flag.Bool("shuffle", false, "Set to true if questions should be shuffled")
	flag.Parse()

	// Open file. Be aware that csvFilename is referenced as a pointer, as you want the value from the flag.
	file, err := os.Open(*csvFilename)
	if err != nil{
		fmt.Printf("Failed to open the CSV file: %s\n", *csvFilename)
		os.Exit(1)
	}

	// Create new CSV reader and save as "r"
	r := csv.NewReader(file)

	// Read lines of CSV, save in "lines"
	lines, err := r.ReadAll()
	if err != nil{
		fmt.Println("Failed to provide CSV file.")
		os.Exit(1)
	}

	// Use parseLines function to read the lines of the CSV file and save in problems struct
	problems := parseLines(lines)

	// Shuffle question order if flag is set to true
	if *shuffleQs == true {
		rand.Seed(time.Now().UnixNano()) // Use seed to randomise pseudo random sequence
		rand.Shuffle(len(problems), func(i, j int) {
			problems[i], problems[j] = problems[j], problems[i]
		})
	}


	// Welcome screen
	fmt.Println("Welcome to the Quiz!")
	fmt.Printf("Time will run out after %d seconds\n", *timeLimit)
	fmt.Println("Press 'enter' to start")
	var start string
	_,_ = fmt.Scanf("\n", &start)

	// Create new timer
	timer := time.NewTimer(time.Duration(*timeLimit) * time.Second)

	// Counter for correct answers in quiz
	correct := 0

	// Present the user with each of the questions
	problemLoop:
		for i, p := range problems {
			fmt.Printf("Problem #%d of #%d: %s = ", i+1, len(problems), p.q)

			answerCh := make(chan string)

			go func() {
				var answer string
				// Use ScanF to read user input. (This should be extended)
				_,_ = fmt.Scanf("%s\n", &answer)
				answerCh <- answer
			}()

			select {
			// Case1: If timer runs out, break the problemLoop Label
			case <-timer.C:
				fmt.Println()
				break problemLoop
			// Case2: If answer is updated by answerCh, then check the answer and adjust correct.
			case answer := <-answerCh:
				if answer == p.a {
					fmt.Println("Correct!")
					correct++
				} else {
					fmt.Println("Wrong :-(")
				}
			}
		}

	// Print final score
	fmt.Printf("You scored %d out of %d \n", correct, len(problems))
}


func parseLines(lines [][]string) []problem {
	ret := make([]problem, len(lines))

	for i, line := range lines {
		ret[i] = problem{
			q: strings.TrimSpace(line[0]),
			a: strings.TrimSpace(line[1]),
		}
	}
	return ret
}



type problem struct {
	q string
	a string
}