# Quiz Game
This Go programme is created for learning the Go programming language. 
The purpose of the programme is to create a simple quiz programme, presented to the user through a CL interface. 
It is inspired by: https://github.com/gophercises/quiz

## Further development 
1. Add string trimming and cleanup to help ensure that correct answers with extra whitespace, capitalization, etc are not considered incorrect. Hint: Check out the strings package.

2. Add an option (a new flag) to shuffle the quiz order each time it is run. [IMPLEMENTED]


